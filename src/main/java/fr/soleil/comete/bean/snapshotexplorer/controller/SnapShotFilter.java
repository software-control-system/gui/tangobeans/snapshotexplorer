package fr.soleil.comete.bean.snapshotexplorer.controller;

import java.util.Collection;
import java.util.Date;

import fr.soleil.comete.bean.snapshotexplorer.model.ExplorerModel;
import fr.soleil.comete.bean.snapshotexplorer.model.SnapShotModel;

public class SnapShotFilter implements ISnapShotFilter {

    private final FilterOperation filterOperation;

    private final ComparedField comparedField;

    private final Object comparedValue;

    public SnapShotFilter(final FilterOperation filterOperation, final ComparedField comparedField,
            final Object comparedValue) {
        super();
        this.filterOperation = filterOperation;
        this.comparedField = comparedField;
        this.comparedValue = comparedValue;
    }

    @Override
    public FilterOperation getFilterOperation() {
        return filterOperation;
    }

    @Override
    public ComparedField getComparedField() {
        return comparedField;
    }

    @Override
    public Object getComparedValue() {
        return comparedValue;
    }

    @Override
    public boolean isMatchingFilter(ExplorerModel model) {
        boolean match = false;
        if (model instanceof SnapShotModel) {
            SnapShotModel snapShotModel = (SnapShotModel) model;
            switch (getComparedField()) {
                case Description_FIELD:
                    String snapShotDescription = snapShotModel.getDescription();
                    match = isMatchingFilter(snapShotDescription);
                    break;
                case Id_FIELD:
                    int snapShotID = snapShotModel.getId();
                    match = isMatchingFilter(snapShotID);
                    break;
                case Date_FIELD:
                    Date snapShotDate = snapShotModel.getDate();
                    match = isMatchingFilter(snapShotDate);
                    break;
                default:
                    break;
            }
        }
        return match;
    }

    protected boolean isMatchingFilter(String testValue) {
        boolean matching = false;
        if ((testValue != null) && (comparedValue != null)) {
            String compareValueString = comparedValue.toString();
            switch (filterOperation) {
                case EQUALS:
                    matching = testValue.equalsIgnoreCase(compareValueString);
                    break;
                case NOT_EQUALS:
                    matching = !testValue.equalsIgnoreCase(compareValueString);
                    break;
                case CONTAINS:
                    matching = testValue.toLowerCase().contains(compareValueString.toLowerCase());
                    break;
                default:
                    break;
            }
        } else if ((testValue == null) && (comparedValue == null)) {
            matching = true;
        }
        return matching;
    }

    protected boolean isMatchingFilter(Date date) {
        boolean result = false;
        if ((comparedValue != null) && (date != null)) {
            if (comparedValue instanceof Date) {
                Date otherDate = (Date) comparedValue;
                switch (filterOperation) {
                    case EQUALS:
                        result = otherDate.equals(date);
                        break;
                    case NOT_EQUALS:
                        result = !otherDate.equals(date);
                        break;
                    case GREATER:
                        result = otherDate.after(date);
                        break;
                    case GREATER_EQUAL:
                        result = (otherDate.compareTo(date) >= 0);
                        break;
                    case LESS:
                        result = otherDate.before(date);
                        break;
                    case LESS_EQUAL:
                        result = (otherDate.compareTo(date) <= 0);
                        break;
                    default:
                        break;
                }
            } else if (comparedValue instanceof Collection<?>) {
                Collection<?> compareDate = (Collection<?>) comparedValue;
                switch (filterOperation) {
                    case BETWEEN:
                        for (Object dateMatch : compareDate) {
                            if ((dateMatch != null) && dateMatch.equals(date)) {
                                result = true;
                                break;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        } else if ((comparedValue == null) && (date == null)) {
            result = true;
        }
        return result;
    }

    protected boolean isMatchingFilter(int id) {
        boolean result = false;
        if ((comparedValue != null) && (id > 0)) {
            if (comparedValue instanceof Integer) {
                Integer compareId = (Integer) comparedValue;
                switch (filterOperation) {
                    case EQUALS:
                        result = compareId == id;
                        break;
                    case NOT_EQUALS:
                        result = compareId != id;
                        break;
                    case GREATER:
                        result = id > compareId;
                        break;
                    case GREATER_EQUAL:
                        result = id >= compareId;
                        break;
                    case LESS:
                        result = id < compareId;
                        break;
                    case LESS_EQUAL:
                        result = id <= compareId;
                        break;
                    default:
                        break;
                }
            }
        } else if ((comparedValue == null) && (id == 0)) {
            result = true;
        }
        return result;
    }
}
