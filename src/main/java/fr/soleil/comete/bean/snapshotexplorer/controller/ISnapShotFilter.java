package fr.soleil.comete.bean.snapshotexplorer.controller;

import fr.soleil.comete.bean.snapshotexplorer.model.ExplorerModel;

public interface ISnapShotFilter {

    public static enum ComparedField {
        Id_FIELD, Date_FIELD, Description_FIELD, Name_FIELD, Author_FIELD, Reason_FIELD;

        @Override
        public String toString() {
            String name = super.toString().replaceFirst("_FIELD", "");
            return name;
        };
    }
    
    public static enum ComparedSnapField {
        Id_FIELD, Date_FIELD, Description_FIELD;

        @Override
        public String toString() {
            String name = super.toString().replaceFirst("_FIELD", "");
            return name;
        };
    }

    public enum FilterOperation {
        EQUALS, NOT_EQUALS, CONTAINS, GREATER, GREATER_EQUAL, LESS, LESS_EQUAL, BETWEEN;
    }

    public FilterOperation getFilterOperation();

    public ComparedField getComparedField();

    public Object getComparedValue();

    public boolean isMatchingFilter(final ExplorerModel model);

}
