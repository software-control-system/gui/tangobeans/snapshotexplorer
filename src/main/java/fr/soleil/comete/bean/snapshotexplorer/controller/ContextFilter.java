package fr.soleil.comete.bean.snapshotexplorer.controller;

import java.util.Date;

import fr.soleil.comete.bean.snapshotexplorer.model.ContextModel;
import fr.soleil.comete.bean.snapshotexplorer.model.ExplorerModel;

public class ContextFilter extends SnapShotFilter {

    public ContextFilter(FilterOperation filterOperation, ComparedField comparedField, Object comparedValue) {
        super(filterOperation, comparedField, comparedValue);
    }

    @Override
    public boolean isMatchingFilter(ExplorerModel model) {
        boolean match = false;
        if (model instanceof ContextModel) {
            ContextModel contextModel = (ContextModel) model;
            switch (getComparedField()) {
                case Name_FIELD:
                    String contextName = contextModel.getContextName();
                    match = isMatchingFilter(contextName);
                    break;
                case Id_FIELD:
                    int contextID = contextModel.getId();
                    match = isMatchingFilter(contextID);
                    break;
                case Description_FIELD:
                    String contextDescrption = contextModel.getDescription();
                    match = isMatchingFilter(contextDescrption);
                    break;
                case Date_FIELD:
                    Date contextDate = contextModel.getDate();
                    match = isMatchingFilter(contextDate);
                    break;
                case Author_FIELD:
                    String contextAuthor = contextModel.getContextAuthor();
                    match = isMatchingFilter(contextAuthor);
                    break;
                case Reason_FIELD:
                    String contextReason = contextModel.getContextReason();
                    match = isMatchingFilter(contextReason);
                    break;
            }
        }
        return match;
    }
}
