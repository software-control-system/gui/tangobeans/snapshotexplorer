package fr.soleil.comete.bean.snapshotexplorer.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jdesktop.swingx.JXDatePicker;

import fr.soleil.comete.bean.snapshotexplorer.controller.ContextFilter;
import fr.soleil.comete.bean.snapshotexplorer.controller.ISnapShotFilter.ComparedField;
import fr.soleil.comete.bean.snapshotexplorer.controller.ISnapShotFilter.FilterOperation;
import fr.soleil.comete.bean.snapshotexplorer.controller.SnapShotController;
import fr.soleil.comete.bean.snapshotexplorer.controller.SnapShotExplorerException;
import fr.soleil.comete.bean.snapshotexplorer.model.ContextModel;
import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;
import fr.soleil.comete.bean.tangotree.datasource.SnapshotSourceDevice;
import fr.soleil.comete.bean.tangotree.swing.SwingDeviceSelector;
import fr.soleil.comete.definition.widget.util.CometeWidgetFactory;
import fr.soleil.comete.swing.Panel;

public class ContextFilterPanel extends JPanel {

    private static final long serialVersionUID = 3816382169250535438L;

    // Functional Member
    private final List<ContextFilter> contextFilterList = new ArrayList<ContextFilter>();
    private final List<Date> newDate = new ArrayList<Date>();
    private boolean showAttribute = true;

    // GUI Member
    private JPanel layoutPanel;
    private JPanel mainPanel;
    private JPanel buttonPanel;

    private JLabel idLabel;
    private JLabel dateLabel;
    private JLabel authorLabel;
    private JLabel descriptionLabel;
    private JLabel reasonLabel;
    private JLabel nameLabel;
    private JLabel andLabel;
    private JLabel contextForAttLabel;
    private JLabel containsLabel;
    private JLabel filteringLabel;

    private JComboBox<String> idComboBox;
    private JComboBox<String> dateComboBox;
    private JComboBox<String> authorComboBox;
    private JComboBox<String> descriptionComboBox;
    private JComboBox<String> reasonComboBox;
    private JComboBox<String> nameCombobox;

    private JTextField idTextField;
    private JTextField dateTextField;
    private JTextField betweenTextField;
    private JTextField authorTextField;
    private JTextField descriptionTextField;
    private JTextField reasonTextField;
    private JTextField nameTextField;
    private JTextField attributeTextField;

    private JButton buttonApply;
    private JButton buttonReset;
    private JButton attributeTangoTreeButton;

    private String[] ID_ITEM_COMBO;
    private String[] DATE_ITEM_COMBO;
    private String[] DESCRITPION_ITEM_COMBO;

    private JXDatePicker dateChooser;
    private JXDatePicker betweenChooser;

    private ContextFilter filter = null;

    private final ContextTable contextTable;
    private static SwingDeviceSelector archivingSelector;

    private JFrame frameTangoTree;
    private JButton buttonTangoTree;
    private JPanel gridPanelTangoTree;
    private Panel selectOkpanTangoTree;
    private GridBagConstraints gridBagLayout;

    public ContextFilterPanel(ContextTable contextTable) {
        this.contextTable = contextTable;
        createContextFilterPanel();
    }

    private void createContextFilterPanel() {
        // Create context filter panel
        layoutPanel = new JPanel();
        mainPanel = new JPanel();
        buttonPanel = new JPanel();

        mainPanel.setLayout(new BorderLayout());

        buttonApply = new JButton("Apply");
        buttonReset = new JButton("Reset");
        attributeTangoTreeButton = new JButton(SnapShotExplorerUtilities.zoomIcon);

        idLabel = new JLabel("ID : ");
        dateLabel = new JLabel("Date : ");
        authorLabel = new JLabel("Author : ");
        descriptionLabel = new JLabel("Description : ");
        reasonLabel = new JLabel("Reason : ");
        nameLabel = new JLabel("Name : ");
        andLabel = new JLabel(" and ");
        contextForAttLabel = new JLabel("Attribute : ");
        containsLabel = new JLabel("CONTAINS ");
        filteringLabel = new JLabel("Filtering : ");

        ID_ITEM_COMBO = new String[] { "EQUALS", "NOT_EQUALS", "GREATER", "GREATER_EQUAL", "LESS", "LESS_EQUAL" };
        DATE_ITEM_COMBO = new String[] { "BETWEEN", "EQUALS", "NOT_EQUALS", "GREATER", "GREATER_EQUAL", "LESS",
                "LESS_EQUAL" };
        DESCRITPION_ITEM_COMBO = new String[] { "EQUALS", "NOT_EQUALS", "CONTAINS" };

        idComboBox = new JComboBox<>(ID_ITEM_COMBO);
        dateComboBox = new JComboBox<>(DATE_ITEM_COMBO);
        descriptionComboBox = new JComboBox<>(DESCRITPION_ITEM_COMBO);
        authorComboBox = new JComboBox<>(DESCRITPION_ITEM_COMBO);
        reasonComboBox = new JComboBox<>(DESCRITPION_ITEM_COMBO);
        nameCombobox = new JComboBox<>(DESCRITPION_ITEM_COMBO);

        idTextField = new JTextField(15);
        authorTextField = new JTextField(15);
        descriptionTextField = new JTextField(15);
        reasonTextField = new JTextField(15);
        nameTextField = new JTextField(15);
        attributeTextField = new JTextField(15);

        dateChooser = new JXDatePicker();
        dateChooser.setFormats("yyyy-MM-dd HH:mm:ss.SSS");
        for (Component comp : dateChooser.getComponents()) {
            if (comp instanceof JTextField) {
                dateTextField = (JTextField) comp;
            }
        }

        betweenChooser = new JXDatePicker();
        betweenChooser.setFormats("yyyy-MM-dd HH:mm:ss.SSS");
        for (Component comp : betweenChooser.getComponents()) {
            if (comp instanceof JTextField) {
                betweenTextField = (JTextField) comp;
            }
        }

        // Tooltip date format
        dateChooser.setToolTipText("The date format is : yyyy-MM-dd HH:mm:ss.SSS");
        betweenChooser.setToolTipText("The date format is : yyyy-MM-dd HH:mm:ss.SSS");

        layoutPanel.setLayout(new GridBagLayout());
        gridBagLayout = new GridBagConstraints();

        // ID GridBagLayout
        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.gridx = 0;
        gridBagLayout.gridy = 0;
        layoutPanel.add(idLabel, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.gridx = 1;
        gridBagLayout.gridy = 0;
        layoutPanel.add(idComboBox, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.gridx = 2;
        gridBagLayout.gridy = 0;
        layoutPanel.add(idTextField, gridBagLayout);

        // Date GridBagLayout
        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 0;
        gridBagLayout.gridy = 1;
        layoutPanel.add(dateLabel, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 1;
        gridBagLayout.gridy = 1;
        layoutPanel.add(dateComboBox, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 2;
        gridBagLayout.gridy = 1;
        layoutPanel.add(dateChooser, gridBagLayout);

        // Auhtor GridBagLayout
        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 0;
        gridBagLayout.gridy = 2;
        layoutPanel.add(authorLabel, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 1;
        gridBagLayout.gridy = 2;
        layoutPanel.add(authorComboBox, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 2;
        gridBagLayout.gridy = 2;
        layoutPanel.add(authorTextField, gridBagLayout);

        // Description GridBagLayout
        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 0;
        gridBagLayout.gridy = 3;
        layoutPanel.add(descriptionLabel, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 1;
        gridBagLayout.gridy = 3;
        layoutPanel.add(descriptionComboBox, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 2;
        gridBagLayout.gridy = 3;
        layoutPanel.add(descriptionTextField, gridBagLayout);

        // Reason GridBagLayout
        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 0;
        gridBagLayout.gridy = 4;
        layoutPanel.add(reasonLabel, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 1;
        gridBagLayout.gridy = 4;
        layoutPanel.add(reasonComboBox, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 2;
        gridBagLayout.gridy = 4;
        layoutPanel.add(reasonTextField, gridBagLayout);

        // Name GridBagLayout
        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 0;
        gridBagLayout.gridy = 5;
        layoutPanel.add(nameLabel, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 1;
        gridBagLayout.gridy = 5;
        layoutPanel.add(nameCombobox, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 2;
        gridBagLayout.gridy = 5;
        layoutPanel.add(nameTextField, gridBagLayout);

        showAttributeFilter(showAttribute);

        String betweenString = dateComboBox.getSelectedItem().toString();
        if (betweenString.contains(FilterOperation.BETWEEN.toString()) && contextTable != null) {
            addBetweenComponent(gridBagLayout);
        }

        dateComboBox.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                String stringEvent = e.getItem().toString();
                if (stringEvent.contains(FilterOperation.BETWEEN.toString())) {
                    addBetweenComponent(gridBagLayout);
                    contextTable.resfreshPanel();
                } else {
                    if ((andLabel != null) && (betweenTextField != null)) {
                        layoutPanel.remove(andLabel);
                        layoutPanel.remove(betweenChooser);
                    }
                }
            }
        });

        buttonApply.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                List<ContextModel> recoverFilterField = recoverFilterField();
                if (recoverFilterField != null && !recoverFilterField.isEmpty()) {
                    contextTable.setData(recoverFilterField);
                    contextFilterList.clear();
                }

                if (!attributeTextField.getText().isEmpty()) {
                    try {
                        contextListForAttribute(recoverFilterField);
                    } catch (SnapShotExplorerException event) {
                        event.printStackTrace();
                    }
                }
            }
        });

        buttonReset.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                descriptionTextField.setText("");
                idTextField.setText("");
                authorTextField.setText("");
                dateTextField.setText("");
                nameTextField.setText("");
                reasonTextField.setText("");
                betweenTextField.setText("");
                attributeTextField.setText("");
                contextTable.resetData();
            }
        });

        attributeTangoTreeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                buildTangoTreeFrame();
            }
        });

        buttonPanel.add(filteringLabel);
        buttonPanel.add(buttonApply);
        buttonPanel.add(buttonReset);

        mainPanel.add(layoutPanel, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        add(mainPanel);

    }

    public void showAttributeFilter(boolean showAttributesFilter) {
        showAttribute = showAttributesFilter;
        if (showAttributesFilter) {
            if (gridBagLayout != null) {
                gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
                gridBagLayout.weightx = 0.5;
                gridBagLayout.gridx = 0;
                gridBagLayout.gridy = 6;
                layoutPanel.add(contextForAttLabel, gridBagLayout);

                gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
                gridBagLayout.weightx = 0.5;
                gridBagLayout.gridx = 1;
                gridBagLayout.gridy = 6;
                layoutPanel.add(containsLabel, gridBagLayout);

                gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
                gridBagLayout.weightx = 0.5;
                gridBagLayout.gridx = 2;
                gridBagLayout.gridy = 6;
                layoutPanel.add(attributeTextField, gridBagLayout);

                gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
                gridBagLayout.weightx = 0.5;
                gridBagLayout.gridx = 4;
                gridBagLayout.gridy = 6;
                layoutPanel.add(attributeTangoTreeButton, gridBagLayout);

                contextForAttLabel.setToolTipText("Seach context that contains the given attribute");
                attributeTextField.setToolTipText("Seach context that contains the given attribute");
                attributeTangoTreeButton.setToolTipText("It's browser of attributes in tree");
            }

        }
        if (!showAttributesFilter) {
            layoutPanel.remove(contextForAttLabel);
            layoutPanel.remove(containsLabel);
            layoutPanel.remove(attributeTextField);
            layoutPanel.remove(attributeTangoTreeButton);
            this.repaint();
        }
    }

    public void setAttributeFilter(String filterApply) {
        if (filterApply != null && attributeTextField != null) {
            attributeTextField.setText(filterApply);
            buttonApply.doClick();
        }
    }

    private void buildTangoTreeFrame() {
        if (frameTangoTree == null) {
            frameTangoTree = new JFrame();
            gridPanelTangoTree = new JPanel();
            selectOkpanTangoTree = new Panel();
            buttonTangoTree = new JButton("Ok");
        }
        attributeTextField.setText("");
        gridPanelTangoTree.setLayout(new GridLayout(1, 1));

        CometeWidgetFactory.setCurrentPackage(CometeWidgetFactory.CometePackage.SWING);
        selectOkpanTangoTree.setLayout(new BorderLayout());
        String deviceName = contextTable.getController().getDeviceName();
        ISourceDevice archivingSource = new SnapshotSourceDevice(deviceName);

        archivingSelector = new SwingDeviceSelector(archivingSource, true, null, null, null, false);

        selectOkpanTangoTree.add(archivingSelector.getSelectorComponent(), BorderLayout.CENTER);

        buttonTangoTree.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                String selectedAttributeWithTangoTree = getSelectedDevices(archivingSelector);
                if (selectedAttributeWithTangoTree != null) {
                    attributeTextField.setText(selectedAttributeWithTangoTree);
                    frameTangoTree.dispose();
                }

            }
        });
        selectOkpanTangoTree.add(buttonTangoTree, BorderLayout.SOUTH);
        gridPanelTangoTree.add(selectOkpanTangoTree);

        frameTangoTree.add(gridPanelTangoTree);
        frameTangoTree.setSize(600, 600);
        frameTangoTree.setVisible(true);
        frameTangoTree.setLocationRelativeTo(null);
        frameTangoTree.setTitle("Attribute tree");
    }

    protected String getSelectedDevices(final SwingDeviceSelector panselect) {
        String selectedAttributeWithTangoTree = "";
        if (panselect != null) {
            List<String> selectedDevices = panselect.getSelectedDevices();
            if ((selectedDevices != null) && !selectedDevices.isEmpty()) {
                selectedAttributeWithTangoTree = selectedDevices.get(0);
            }
        }
        return selectedAttributeWithTangoTree;
    }

    private List<ContextModel> recoverFilterField() {
        List<ContextModel> contextFilter = null;

        if (!authorTextField.getText().isEmpty()) {
            authorFilter();
        }

        if (!idTextField.getText().isEmpty()) {
            idFilter();
        }

        if (!descriptionTextField.getText().isEmpty()) {
            descriptionFilter();
        }

        if (!dateTextField.getText().isEmpty()) {
            dateFilter();
        }

        if (!reasonTextField.getText().isEmpty()) {
            reasonFilter();
        }

        if (!nameTextField.getText().isEmpty()) {
            nameFilter();
        }

        if (contextFilterList != null) {
            contextFilter = applyFilter(contextFilterList);
        }
        return contextFilter;
    }

    private void authorFilter() {
        String authorField = authorTextField.getText();
        String authorCombo = authorComboBox.getSelectedItem().toString();
        if (authorCombo.equals(FilterOperation.EQUALS.toString())) {
            filter = new ContextFilter(FilterOperation.EQUALS, ComparedField.Author_FIELD, authorField);
        }
        if (authorCombo.equals(FilterOperation.CONTAINS.toString())) {
            filter = new ContextFilter(FilterOperation.CONTAINS, ComparedField.Author_FIELD, authorField);
        }
        if (authorCombo.equals(FilterOperation.NOT_EQUALS.toString())) {
            filter = new ContextFilter(FilterOperation.NOT_EQUALS, ComparedField.Author_FIELD, authorField);
        }
        if (filter != null) {
            contextFilterList.add(filter);
        }
    }

    private void idFilter() {
        int id = Integer.parseInt(idTextField.getText());
        String idCombo = idComboBox.getSelectedItem().toString();
        if (idCombo.equals(FilterOperation.EQUALS.toString())) {
            filter = new ContextFilter(FilterOperation.EQUALS, ComparedField.Id_FIELD, id);
        } else if (idCombo.equals(FilterOperation.CONTAINS.toString())) {
            filter = new ContextFilter(FilterOperation.CONTAINS, ComparedField.Id_FIELD, id);
        } else if (idCombo.equals(FilterOperation.GREATER.toString())) {
            filter = new ContextFilter(FilterOperation.GREATER, ComparedField.Id_FIELD, id);
        } else if (idCombo.equals(FilterOperation.GREATER_EQUAL.toString())) {
            filter = new ContextFilter(FilterOperation.GREATER_EQUAL, ComparedField.Id_FIELD, id);
        } else if (idCombo.equals(FilterOperation.LESS.toString())) {
            filter = new ContextFilter(FilterOperation.LESS, ComparedField.Id_FIELD, id);
        } else if (idCombo.equals(FilterOperation.LESS_EQUAL.toString())) {
            filter = new ContextFilter(FilterOperation.LESS_EQUAL, ComparedField.Id_FIELD, id);
        } else if (idCombo.equals(FilterOperation.NOT_EQUALS.toString())) {
            filter = new ContextFilter(FilterOperation.NOT_EQUALS, ComparedField.Id_FIELD, id);
        }
        if (filter != null) {
            contextFilterList.add(filter);
        }
    }

    private void descriptionFilter() {
        String descriptionText = descriptionTextField.getText();
        String descriptionCombo = descriptionComboBox.getSelectedItem().toString();
        if (descriptionCombo.equals(FilterOperation.EQUALS.toString())) {
            filter = new ContextFilter(FilterOperation.EQUALS, ComparedField.Description_FIELD, descriptionText);
        }
        if (descriptionCombo.equals(FilterOperation.CONTAINS.toString())) {
            filter = new ContextFilter(FilterOperation.CONTAINS, ComparedField.Description_FIELD, descriptionText);
        }
        if (descriptionCombo.equals(FilterOperation.NOT_EQUALS.toString())) {
            filter = new ContextFilter(FilterOperation.NOT_EQUALS, ComparedField.Description_FIELD, descriptionText);
        }
        if (filter != null) {
            contextFilterList.add(filter);
        }
    }

    private void reasonFilter() {
        String reasonText = reasonTextField.getText();
        String reasonCombo = reasonComboBox.getSelectedItem().toString();
        if (reasonCombo.equals(FilterOperation.EQUALS.toString())) {
            filter = new ContextFilter(FilterOperation.EQUALS, ComparedField.Reason_FIELD, reasonText);
        }
        if (reasonCombo.equals(FilterOperation.CONTAINS.toString())) {
            filter = new ContextFilter(FilterOperation.CONTAINS, ComparedField.Reason_FIELD, reasonText);
        }
        if (reasonCombo.equals(FilterOperation.NOT_EQUALS.toString())) {
            filter = new ContextFilter(FilterOperation.NOT_EQUALS, ComparedField.Reason_FIELD, reasonText);
        }
        if (filter != null) {
            contextFilterList.add(filter);
        }
    }

    private void nameFilter() {
        String nameText = nameTextField.getText();
        String nameCombo = nameCombobox.getSelectedItem().toString();
        if (nameCombo.equals(FilterOperation.EQUALS.toString())) {
            filter = new ContextFilter(FilterOperation.EQUALS, ComparedField.Name_FIELD, nameText);
        }
        if (nameCombo.equals(FilterOperation.CONTAINS.toString())) {
            filter = new ContextFilter(FilterOperation.CONTAINS, ComparedField.Name_FIELD, nameText);
        }
        if (nameCombo.equals(FilterOperation.NOT_EQUALS.toString())) {
            filter = new ContextFilter(FilterOperation.NOT_EQUALS, ComparedField.Name_FIELD, nameText);
        }
        if (filter != null) {
            contextFilterList.add(filter);
        }
    }

    private void dateFilter() {
        String dateText = dateTextField.getText();
        String dateCombo = dateComboBox.getSelectedItem().toString();
        try {
            Date date = SnapShotExplorerUtilities.DATE_FORMATER.parse(dateText);
            if (dateCombo.equals(FilterOperation.EQUALS.toString())) {
                filter = new ContextFilter(FilterOperation.EQUALS, ComparedField.Date_FIELD, date);
            }
            if (dateCombo.equals(FilterOperation.NOT_EQUALS.toString())) {
                filter = new ContextFilter(FilterOperation.NOT_EQUALS, ComparedField.Date_FIELD, date);
            }
            if (dateCombo.equals(FilterOperation.GREATER.toString())) {
                filter = new ContextFilter(FilterOperation.GREATER, ComparedField.Date_FIELD, date);
            }
            if (dateCombo.equals(FilterOperation.GREATER_EQUAL.toString())) {
                filter = new ContextFilter(FilterOperation.GREATER_EQUAL, ComparedField.Date_FIELD, date);
            }
            if (dateCombo.equals(FilterOperation.LESS.toString())) {
                filter = new ContextFilter(FilterOperation.LESS, ComparedField.Date_FIELD, date);
            }
            if (dateCombo.equals(FilterOperation.LESS_EQUAL.toString())) {
                filter = new ContextFilter(FilterOperation.LESS_EQUAL, ComparedField.Date_FIELD, date);
            }
            if (dateCombo.equals(FilterOperation.BETWEEN.toString())) {

                if (betweenTextField != null) {
                    newDate.clear();
                    Date betweenDate = null;
                    Date contextDate = null;
                    if (!betweenTextField.getText().isEmpty()) {
                        String betweenDateString = betweenTextField.getText();
                        betweenDate = SnapShotExplorerUtilities.DATE_FORMATER.parse(betweenDateString);
                        contextTable.resetData();
                        List<ContextModel> data = contextTable.getData();
                        for (ContextModel model : data) {
                            contextDate = model.getDate();
                            if (contextDate.after(date) && contextDate.before(betweenDate)) {
                                newDate.add(contextDate);
                            }
                        }
                    }
                }
                if (newDate != null) {
                    filter = new ContextFilter(FilterOperation.BETWEEN, ComparedField.Date_FIELD, newDate);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (filter != null) {
            contextFilterList.add(filter);
        }
    }

    private void contextListForAttribute(List<ContextModel> contextFilter) throws SnapShotExplorerException {
        String contextForAttText = attributeTextField.getText();
        SnapShotController controller = contextTable.getController();
        List<ContextModel> contextListForAttribute = null;
        List<ContextModel> contextListForAttributeWithFilter = new ArrayList<ContextModel>();

        if (contextFilter != null && !contextFilter.isEmpty()) {
            contextListForAttribute = controller.getContextListForAttribute(contextForAttText);
            for (ContextModel model : contextListForAttribute) {
                for (ContextModel modelFilter : contextFilter) {
                    modelFilter.getId();
                    if (model.getId() == modelFilter.getId()) {
                        contextListForAttributeWithFilter.add(model);
                    }
                }
                contextTable.setData(contextListForAttributeWithFilter);
            }
        } else {
            contextListForAttribute = controller.getContextListForAttribute(contextForAttText);
            contextTable.setData(contextListForAttribute);
        }
    }

    private void addBetweenComponent(GridBagConstraints gridBagLayout) {
        if ((gridBagLayout != null) && (layoutPanel != null)) {
            gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
            gridBagLayout.weightx = 0.5;
            gridBagLayout.gridx = 4;
            gridBagLayout.gridy = 1;
            layoutPanel.add(andLabel, gridBagLayout);

            gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
            gridBagLayout.weightx = 0.5;
            gridBagLayout.gridx = 6;
            gridBagLayout.gridy = 1;
            layoutPanel.add(betweenChooser, gridBagLayout);
        }
    }

    private List<ContextModel> applyFilter(List<ContextFilter> filter) {
        SnapShotController controller = contextTable.getController();
        List<ContextFilter> filterContext = new ArrayList<ContextFilter>();
        List<ContextModel> data = new ArrayList<ContextModel>();
        if (filter != null && !filter.isEmpty()) {
            filterContext = filter;
            try {
                if (controller != null) {
                    data = controller.getAllContext(filterContext);
                }
            } catch (SnapShotExplorerException e) {
                e.printStackTrace();
            }
        }
        return data;
    }
}
