package fr.soleil.comete.bean.snapshotexplorer.controller;

public class SnapShotExplorerException extends Exception {

    private static final long serialVersionUID = -632498623665128407L;

    public SnapShotExplorerException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public SnapShotExplorerException(final String message) {
        super(message);
    }

    public SnapShotExplorerException(final Throwable cause) {
        super(cause);
    }
}
