package fr.soleil.comete.bean.snapshotexplorer.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevVarLongStringArray;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.snapshotexplorer.controller.SnapShotExplorerException;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

public class SnapShotExtractorDevice {

    private static final String DEVICE_CLASS = "SnapExtractor";
    private static final String GETALLCONTEXT_CMD = "GetAllContexts";
    private static final String CONTEXT_SEP = "\\|";
    private static final String SNAPSHOT_SEP = ",";
    private static final String GETSNAPSFORCONTEXT_CMD = "GetSnapsForContext";
    private static final String GETATTRIBUTELISTFORCONTEXT_CMD = "GetAttributeListForContext";
    private static final String GETCONTEXTLISTFORATTRIBUTE_CMD = "GetContextListForAttribute";
    private static final String GETSNAPSHOTATTRIBUTELIST_CMD = "GetSnapshotAttributeList";
    public static final SimpleDateFormat DATE_FORMATER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    private static final int CONTEXT_NAME_INDEX = 0;
    private static final int CONTEXT_AUTHOR_INDEX = 1;
    private static final int CONTEXT_DATE_INDEX = 2;
    private static final int CONTEXT_REASON_INDEX = 3;
    private static final int CONTEXT_DESCRIPTION_INDEX = 4;

    private static final int SNAPSHOT_DATE_INDEX = 0;
    private static final int SNAPSHOT_COMMENT_INDEX = 1;

    private final String deviceName;

    public SnapShotExtractorDevice(final String deviceName) {
        super();
        this.deviceName = deviceName;
    }

    public List<ContextModel> getAllContext() throws SnapShotExplorerException {
        List<ContextModel> contextList = new ArrayList<ContextModel>();
        try {
            DeviceProxy deviceProxy = TangoDeviceHelper.getDeviceProxy(deviceName);
            if (deviceProxy != null) {
                DeviceData command_inout = deviceProxy.command_inout(GETALLCONTEXT_CMD);
                DevVarLongStringArray extractLongStringArray = command_inout.extractLongStringArray();

                int[] contextId = extractLongStringArray.lvalue;
                String[] contextInfo = extractLongStringArray.svalue;

                ContextModel contextModel = null;
                if ((contextId != null) && (contextInfo != null) && (contextId.length == contextInfo.length)) {

                    for (int i = 0; i < contextId.length; i++) {
                        contextModel = createContextModel(contextId[i], contextInfo[i]);
                        if (contextModel != null) {
                            contextList.add(contextModel);
                        }
                    }
                }
            }

        } catch (DevFailed e) {
            throw new SnapShotExplorerException(DevFailedUtils.toString(e), e);
        }
        return contextList;
    }
    
    public static boolean isSnapShotExtractorAvailable(){
        boolean available = false;
        String[] deviceNameList = getDeviceNameList();
        if(deviceNameList != null && deviceNameList.length > 0){
            String deviceName = deviceNameList[0];
            available = isSnapShotExtractorDeviceAvailable(deviceName);
        }
        return available;
    }
    
    public static boolean isSnapShotExtractorDeviceAvailable(String deviceName){
        return  TangoDeviceHelper.isDeviceRunning(deviceName);
    }

    private ContextModel createContextModel(int id, String contextInfo) {
        ContextModel model = null;
        if (contextInfo != null) {
            String[] split = contextInfo.split(CONTEXT_SEP);
            String tmpContextName = null;
            String tmpContextAuthor = null;
            Date tmpContextDate = null;
            String tmpContextReason = null;
            String tmpContextDescritpion = null;

            if (split.length > 0) {
                tmpContextName = split[CONTEXT_NAME_INDEX];
            }

            if (split.length > 1) {
                tmpContextAuthor = split[CONTEXT_AUTHOR_INDEX];
            }

            if (split.length > 2) {
                try {
                    tmpContextDate = DATE_FORMATER.parse(split[CONTEXT_DATE_INDEX]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (split.length > 3) {
                tmpContextReason = split[CONTEXT_REASON_INDEX];
            }
            if (split.length > 4) {
                tmpContextDescritpion = split[CONTEXT_DESCRIPTION_INDEX];
            }
            model = new ContextModel(id, tmpContextDescritpion, tmpContextDate, tmpContextName, tmpContextAuthor,
                    tmpContextReason);
        }

        return model;
    }

    public List<SnapShotModel> getSnapsForContext(int id) throws SnapShotExplorerException {
        List<SnapShotModel> snapShotList = new ArrayList<SnapShotModel>();
        try {

            DeviceProxy deviceProxy = TangoDeviceHelper.getDeviceProxy(deviceName);
            if (deviceProxy != null) {
                DeviceData argin = new DeviceData();
                argin.insert(id);

                DeviceData command_inout = deviceProxy.command_inout(GETSNAPSFORCONTEXT_CMD, argin);
                DevVarLongStringArray extractLongStringArray = command_inout.extractLongStringArray();

                int[] snapShotId = extractLongStringArray.lvalue;
                String[] snapShotInfo = extractLongStringArray.svalue;

                SnapShotModel snapShotModel = null;
                if ((snapShotId != null) && (snapShotInfo != null) && (snapShotId.length == snapShotInfo.length)) {

                    for (int i = 0; i < snapShotId.length; i++) {
                        snapShotModel = createSnapShotModel(snapShotId[i], snapShotInfo[i]);
                        if (snapShotModel != null) {
                            snapShotList.add(snapShotModel);
                        }
                    }
                }
            }
        } catch (DevFailed e) {
            throw new SnapShotExplorerException(DevFailedUtils.toString(e), e);
        }
        return snapShotList;
    }

    public List<String> getAttributeListForContext(int contextID) throws SnapShotExplorerException {
        List<String> attributeList = new ArrayList<String>();
        DeviceProxy deviceProxy = TangoDeviceHelper.getDeviceProxy(deviceName);
        if (deviceProxy != null) {
            try {
                DeviceData argin = new DeviceData();

                if (contextID > 0) {
                    argin.insert(contextID);
                }
                DeviceData command_inout = deviceProxy.command_inout(GETATTRIBUTELISTFORCONTEXT_CMD, argin);
                String[] extractStringArray = command_inout.extractStringArray();
                for(String attribute : extractStringArray){
                    attributeList.add(attribute);
                }
            } catch (DevFailed e) {
               // e.printStackTrace();
            }
        }
        return attributeList;
    }

    public List<ContextModel> getContextListForAttribute(String attributeName) {
        List<ContextModel> contextList = new ArrayList<ContextModel>();
        DeviceProxy deviceProxy = TangoDeviceHelper.getDeviceProxy(deviceName);
        if(deviceProxy != null) {
            try {
                DeviceData argin = new DeviceData();
                if(attributeName != null) {
                    argin.insert(attributeName);
                }
                DeviceData command_inout = deviceProxy.command_inout(GETCONTEXTLISTFORATTRIBUTE_CMD, argin);
                DevVarLongStringArray extractLongStringArray = command_inout.extractLongStringArray();

                int[] contextId = extractLongStringArray.lvalue;
                String[] contextInfo = extractLongStringArray.svalue;

                ContextModel contextModel = null;
                if ((contextId != null) && (contextInfo != null) && (contextId.length == contextInfo.length)) {

                    for (int i = 0; i < contextId.length; i++) {
                        contextModel = createContextModel(contextId[i], contextInfo[i]);
                        if (contextModel != null) {
                            contextList.add(contextModel);
                        }
                    }
                }

            } catch (DevFailed e) {

            }
        }
        return contextList;
    }

    public List<String> getSnapshotAttributeList() {
        List<String> snapshotAttributeList = new ArrayList<String>();
        DeviceProxy deviceProxy = TangoDeviceHelper.getDeviceProxy(deviceName);
        if (deviceProxy != null) {
            try {
                DeviceData command_inout = deviceProxy.command_inout(GETSNAPSHOTATTRIBUTELIST_CMD);
                String[] extractStringArray = command_inout.extractStringArray();
                for (String snapshotAttribute : extractStringArray) {
                    snapshotAttributeList.add(snapshotAttribute);
                }
            } catch (DevFailed e) {
                e.printStackTrace();
            }
        }
        return snapshotAttributeList;
    }

    private SnapShotModel createSnapShotModel(int id, String snapShotInfo) {
        SnapShotModel model = null;

        if (snapShotInfo != null && id > -1) {
            String[] split = snapShotInfo.split(SNAPSHOT_SEP);
            Date tmpSnapShotDate = null;
            String tmpSnapShotComment = null;

            if (split.length > 0) {
                try {
                    tmpSnapShotDate = DATE_FORMATER.parse(split[SNAPSHOT_DATE_INDEX]);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (split.length > 1) {
                tmpSnapShotComment = split[SNAPSHOT_COMMENT_INDEX];
            }
            model = new SnapShotModel(id, tmpSnapShotComment, tmpSnapShotDate);
        }
        return model;
    }

    public static String[] getDeviceNameList() {
        String[] deviceList = null;
        Database database = TangoDeviceHelper.getDatabase();
        if (database != null) {
            try {
                deviceList = database.get_device_exported_for_class(DEVICE_CLASS);
            } catch (DevFailed e) {
            }
        }
        return deviceList;
    }
    
    public static String getFirstAvailableDeviceName(){
        String deviceName = null;
        String[] deviceList = getDeviceNameList();
        if(deviceList != null && deviceList.length > 0){
            deviceName = deviceList[0];
        }
        return deviceName;
    }

}
