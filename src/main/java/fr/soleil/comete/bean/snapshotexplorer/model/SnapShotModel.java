package fr.soleil.comete.bean.snapshotexplorer.model;

import java.util.Date;

public class SnapShotModel extends ExplorerModel {

    public SnapShotModel(int id, String description, Date date) {
        super(id, description, date);
    }
}
