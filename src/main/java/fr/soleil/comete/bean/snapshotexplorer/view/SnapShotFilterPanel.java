package fr.soleil.comete.bean.snapshotexplorer.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jdesktop.swingx.JXDatePicker;

import fr.soleil.comete.bean.snapshotexplorer.controller.ISnapShotFilter.ComparedField;
import fr.soleil.comete.bean.snapshotexplorer.controller.ISnapShotFilter.FilterOperation;
import fr.soleil.comete.bean.snapshotexplorer.controller.SnapShotController;
import fr.soleil.comete.bean.snapshotexplorer.controller.SnapShotExplorerException;
import fr.soleil.comete.bean.snapshotexplorer.controller.SnapShotFilter;
import fr.soleil.comete.bean.snapshotexplorer.model.SnapShotModel;

public class SnapShotFilterPanel extends JPanel {

    private static final long serialVersionUID = -2230469863129758739L;

    // Functional member
    private final List<SnapShotFilter> snapShotfilterList = new ArrayList<SnapShotFilter>();

    // GUI Member
    private JPanel layoutPanel;
    private JPanel mainPanel;
    private JPanel buttonPanel;

    private JLabel idLabel;
    private JLabel dateLabel;
    private JLabel andLabel;
    private JLabel descriptionLabel;
    private JLabel filteringLabel;

    private JComboBox<String> idComboBox;
    private JComboBox<String> dateComboBox;
    private JComboBox<String> descriptionComboBox;

    private JTextField idTextField;
    private JTextField dateTextField;
    private JTextField descriptionTextField;
    private JTextField betweenTextField;

    private JButton buttonOk;
    private JButton buttonReset;

    private String[] ID_ITEM_COMBO;
    private String[] DATE_ITEM_COMBO;
    private String[] COMMENT_ITEM_COMBO;

    private SnapShotFilter filter = null;

    private JXDatePicker dateChooser;
    private JXDatePicker betweenChooser;

    private final SnapShotTable snapShotTable;

    public SnapShotFilterPanel(SnapShotTable snapShotTable) {
        this.snapShotTable = snapShotTable;
        createSnapShotFilterPanel();
    }

    private void createSnapShotFilterPanel() {
        // Create Snapshot filter panel
        layoutPanel = new JPanel();
        mainPanel = new JPanel();
        buttonPanel = new JPanel();

        mainPanel.setLayout(new BorderLayout());

        buttonOk = new JButton("Apply");
        buttonReset = new JButton("Reset");

        idLabel = new JLabel("ID : ");
        dateLabel = new JLabel("Date : ");
        descriptionLabel = new JLabel("Description : ");
        andLabel = new JLabel(" and ");
        filteringLabel = new JLabel("Filtering : ");

        ID_ITEM_COMBO = new String[] { "EQUALS", "NOT_EQUALS", "GREATER", "GREATER_EQUAL", "LESS", "LESS_EQUAL" };
        DATE_ITEM_COMBO = new String[] { "BETWEEN", "EQUALS", "NOT_EQUALS", "GREATER", "GREATER_EQUAL", "LESS",
                "LESS_EQUAL" };
        COMMENT_ITEM_COMBO = new String[] { "EQUALS", "NOT_EQUALS", "CONTAINS" };

        idComboBox = new JComboBox<>(ID_ITEM_COMBO);
        dateComboBox = new JComboBox<>(DATE_ITEM_COMBO);
        descriptionComboBox = new JComboBox<>(COMMENT_ITEM_COMBO);

        idTextField = new JTextField(15);
        descriptionTextField = new JTextField(15);

        dateChooser = new JXDatePicker();
        dateChooser.setToolTipText("The date format is : yyyy-MM-dd HH:mm:ss.SSS");
        dateChooser.setFormats("yyyy-MM-dd HH:mm:ss.SSS");
        for (Component comp : dateChooser.getComponents()) {
            if (comp instanceof JTextField) {
                dateTextField = (JTextField) comp;
            }
        }

        betweenChooser = new JXDatePicker();
        dateChooser.setToolTipText("The date format is : yyyy-MM-dd HH:mm:ss.SSS");
        betweenChooser.setFormats("yyyy-MM-dd HH:mm:ss.SSS");
        for (Component comp : betweenChooser.getComponents()) {
            if (comp instanceof JTextField) {
                betweenTextField = (JTextField) comp;
            }
        }

        layoutPanel.setLayout(new GridBagLayout());
        final GridBagConstraints gridBagLayout = new GridBagConstraints();

        // ID GridBagLayout
        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.gridx = 0;
        gridBagLayout.gridy = 0;
        layoutPanel.add(idLabel, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.gridx = 1;
        gridBagLayout.gridy = 0;
        layoutPanel.add(idComboBox, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.gridx = 2;
        gridBagLayout.gridy = 0;
        layoutPanel.add(idTextField, gridBagLayout);

        // Date GridBagLayout
        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 0;
        gridBagLayout.gridy = 1;
        layoutPanel.add(dateLabel, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 1;
        gridBagLayout.gridy = 1;
        layoutPanel.add(dateComboBox, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 2;
        gridBagLayout.gridy = 1;
        layoutPanel.add(dateChooser, gridBagLayout);

        // Comment GridBagLayout
        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 0;
        gridBagLayout.gridy = 2;
        layoutPanel.add(descriptionLabel, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 1;
        gridBagLayout.gridy = 2;
        layoutPanel.add(descriptionComboBox, gridBagLayout);

        gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
        gridBagLayout.weightx = 0.5;
        gridBagLayout.gridx = 2;
        gridBagLayout.gridy = 2;
        layoutPanel.add(descriptionTextField, gridBagLayout);

        String betweenString = dateComboBox.getSelectedItem().toString();
        if (betweenString.contains(FilterOperation.BETWEEN.toString()) && snapShotTable != null) {
            addBetweenComponent(gridBagLayout);
        }

        dateComboBox.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                String stringEvent = e.getItem().toString();
                if (stringEvent.contains(FilterOperation.BETWEEN.toString())) {
                    addBetweenComponent(gridBagLayout);
                    snapShotTable.resfreshPanel();
                } else {
                    if ((andLabel != null) && (betweenTextField != null)) {
                        layoutPanel.remove(andLabel);
                        layoutPanel.remove(betweenChooser);
                    }
                }
            }
        });

        buttonOk.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                List<SnapShotModel> recoverFilterField = recoverFilterField();
                if (recoverFilterField != null) {
                    snapShotTable.setData(recoverFilterField);
                    snapShotfilterList.clear();
                }
            }
        });

        buttonReset.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                idTextField.setText("");
                dateTextField.setText("");
                descriptionTextField.setText("");
                betweenTextField.setText("");
                snapShotTable.resetData();
            }
        });

        buttonPanel.add(filteringLabel);
        buttonPanel.add(buttonOk);
        buttonPanel.add(buttonReset);

        mainPanel.add(layoutPanel, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        add(mainPanel);
    }

    private List<SnapShotModel> recoverFilterField() {
        List<SnapShotModel> snapShotFilter = null;
        if (!idTextField.getText().isEmpty()) {
            idFilter();
        }
        if (!descriptionTextField.getText().isEmpty()) {
            descriptionFilter();
        }
        if (!dateTextField.getText().isEmpty()) {
            dateFilter();
        }
        if (snapShotfilterList != null) {
            snapShotFilter = applyFilter(snapShotfilterList);
        }
        return snapShotFilter;
    }

    private void idFilter() {
        int id = Integer.parseInt(idTextField.getText());
        String idCombo = idComboBox.getSelectedItem().toString();
        if (idCombo.equals(FilterOperation.EQUALS.toString())) {
            filter = new SnapShotFilter(FilterOperation.EQUALS, ComparedField.Id_FIELD, id);
        } else if (idCombo.equals(FilterOperation.CONTAINS.toString())) {
            filter = new SnapShotFilter(FilterOperation.CONTAINS, ComparedField.Id_FIELD, id);
        } else if (idCombo.equals(FilterOperation.GREATER.toString())) {
            filter = new SnapShotFilter(FilterOperation.GREATER, ComparedField.Id_FIELD, id);
        } else if (idCombo.equals(FilterOperation.GREATER_EQUAL.toString())) {
            filter = new SnapShotFilter(FilterOperation.GREATER_EQUAL, ComparedField.Id_FIELD, id);
        } else if (idCombo.equals(FilterOperation.LESS.toString())) {
            filter = new SnapShotFilter(FilterOperation.LESS, ComparedField.Id_FIELD, id);
        } else if (idCombo.equals(FilterOperation.LESS_EQUAL.toString())) {
            filter = new SnapShotFilter(FilterOperation.LESS_EQUAL, ComparedField.Id_FIELD, id);
        } else if (idCombo.equals(FilterOperation.NOT_EQUALS.toString())) {
            filter = new SnapShotFilter(FilterOperation.NOT_EQUALS, ComparedField.Id_FIELD, id);
        }
        if (filter != null) {
            snapShotfilterList.add(filter);
        }
    }

    private void descriptionFilter() {
        String descriptionText = descriptionTextField.getText();
        String descriptionCombo = descriptionComboBox.getSelectedItem().toString();
        if (descriptionCombo.equals(FilterOperation.EQUALS.toString())) {
            filter = new SnapShotFilter(FilterOperation.EQUALS, ComparedField.Description_FIELD, descriptionText);
        }
        if (descriptionCombo.equals(FilterOperation.NOT_EQUALS.toString())) {
            filter = new SnapShotFilter(FilterOperation.NOT_EQUALS, ComparedField.Description_FIELD, descriptionText);
        }
        if (descriptionCombo.equals(FilterOperation.CONTAINS.toString())) {
            filter = new SnapShotFilter(FilterOperation.CONTAINS, ComparedField.Description_FIELD, descriptionText);
        }
        if (filter != null) {
            snapShotfilterList.add(filter);
        }
    }

    private void dateFilter() {
        String dateText = dateTextField.getText();
        String dateCombo = dateComboBox.getSelectedItem().toString();
        try {
            Date date = SnapShotExplorerUtilities.DATE_FORMATER.parse(dateText);
            if (dateCombo.equals(FilterOperation.EQUALS.toString())) {
                filter = new SnapShotFilter(FilterOperation.EQUALS, ComparedField.Date_FIELD, date);
            }
            if (dateCombo.equals(FilterOperation.NOT_EQUALS.toString())) {
                filter = new SnapShotFilter(FilterOperation.NOT_EQUALS, ComparedField.Date_FIELD, date);
            }
            if (dateCombo.equals(FilterOperation.GREATER.toString())) {
                filter = new SnapShotFilter(FilterOperation.GREATER, ComparedField.Date_FIELD, date);
            }
            if (dateCombo.equals(FilterOperation.GREATER_EQUAL.toString())) {
                filter = new SnapShotFilter(FilterOperation.GREATER_EQUAL, ComparedField.Date_FIELD, date);
            }
            if (dateCombo.equals(FilterOperation.LESS.toString())) {
                filter = new SnapShotFilter(FilterOperation.LESS, ComparedField.Date_FIELD, date);
            }
            if (dateCombo.equals(FilterOperation.LESS_EQUAL.toString())) {
                filter = new SnapShotFilter(FilterOperation.LESS_EQUAL, ComparedField.Date_FIELD, date);
            }
            if (dateCombo.equals(FilterOperation.BETWEEN.toString())) {
                List<Date> newDate = new ArrayList<Date>();
                if (betweenTextField != null) {
                    Date betweenDate = null;
                    Date snapDate = null;
                    if (!betweenTextField.getText().isEmpty()) {
                        String betweenDateString = betweenTextField.getText();
                        betweenDate = SnapShotExplorerUtilities.DATE_FORMATER.parse(betweenDateString);
                        snapShotTable.resetData();
                        List<SnapShotModel> data = snapShotTable.getData();
                        for (SnapShotModel model : data) {
                            snapDate = model.getDate();
                            if (snapDate.after(date) && snapDate.before(betweenDate)) {
                                newDate.add(snapDate);
                            }
                        }
                    }
                }
                if (newDate != null) {
                    filter = new SnapShotFilter(FilterOperation.BETWEEN, ComparedField.Date_FIELD, newDate);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (filter != null) {
            snapShotfilterList.add(filter);
        }
    }

    private void addBetweenComponent(GridBagConstraints gridBagLayout) {
        if ((gridBagLayout != null) && (layoutPanel != null)) {
            gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
            gridBagLayout.weightx = 0.5;
            gridBagLayout.gridx = 3;
            gridBagLayout.gridy = 1;
            layoutPanel.add(andLabel, gridBagLayout);

            gridBagLayout.fill = GridBagConstraints.HORIZONTAL;
            gridBagLayout.weightx = 0.5;
            gridBagLayout.gridx = 4;
            gridBagLayout.gridy = 1;
            layoutPanel.add(betweenChooser, gridBagLayout);
        }
    }

    private List<SnapShotModel> applyFilter(List<SnapShotFilter> filter) {
        SnapShotController controller = snapShotTable.getController();
        List<SnapShotFilter> filterContext = new ArrayList<SnapShotFilter>();
        List<SnapShotModel> data = new ArrayList<SnapShotModel>();
        if (filter != null) {
            filterContext = filter;
            if (controller != null) {
                try {
                    data = controller.getAllSnapsForContext(snapShotTable.getContextID(), filterContext);
                } catch (SnapShotExplorerException e) {
                    e.printStackTrace();
                }
            }
        }
        return data;
    }
}
