package fr.soleil.comete.bean.snapshotexplorer.controller;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.comete.bean.snapshotexplorer.controller.ISnapShotFilter.ComparedField;
import fr.soleil.comete.bean.snapshotexplorer.controller.ISnapShotFilter.FilterOperation;
import fr.soleil.comete.bean.snapshotexplorer.model.ContextModel;
import fr.soleil.comete.bean.snapshotexplorer.model.SnapShotExtractorDevice;
import fr.soleil.comete.bean.snapshotexplorer.model.SnapShotModel;
import fr.soleil.comete.bean.snapshotexplorer.view.ErrorMessageFrame;

public class SnapShotController {

    private final SnapShotExtractorDevice snapShotDevice;
    private String deviceName;
    private static int rackCount = 1;
    private ErrorMessageFrame errorMessageFrame = new ErrorMessageFrame();

    public SnapShotController(final String deviceModel) {
        deviceName = deviceModel;
        snapShotDevice = new SnapShotExtractorDevice(deviceModel);
    }
    
    public String getDeviceName(){
        return deviceName;
    }

    public List<ContextModel> getAllContext() {
        List<ContextModel> allContextList = null;
        try {
            allContextList = snapShotDevice.getAllContext();
        } catch (SnapShotExplorerException e) {
            errorMessageFrame.newErrorDetected(e, true);
            errorMessageFrame.newMessageDetected("Error with the command getAllContext \n", true);   
        }
        return allContextList;
    }

    public ContextModel getContext(int id)  {
        ContextModel model = null;
        List<ContextModel> allContextList;
        try {
            allContextList = snapShotDevice.getAllContext();

        for (ContextModel ctxtModel : allContextList) {
            if (ctxtModel.getId() == id) {
                model = ctxtModel;
            }
        }        } catch (SnapShotExplorerException e) {
            errorMessageFrame.newErrorDetected(e, true);
            errorMessageFrame.newMessageDetected("Error with the command getContext \n" + "id = " + id, true);      
        }
        return model;
    }

    public List<ContextModel> getAllContext(List<ContextFilter> filter) throws SnapShotExplorerException {
        List<ContextModel> allContext = getAllContext();
        List<ContextModel> filteredContext = new ArrayList<ContextModel>();
        for (ContextModel contextModel : allContext) {
            boolean matchingFilter = true;
            for (ContextFilter contextFilter : filter) {
                matchingFilter = contextFilter.isMatchingFilter(contextModel);
                if (!matchingFilter) {
                    break;
                }
            }
            if (matchingFilter) {
                filteredContext.add(contextModel);
            }
        }
        return filteredContext;
    }

    public List<SnapShotModel> getAllSnapsForContext(int contextId){
        List<SnapShotModel> snapsForContext = null;
        try {
            snapsForContext = snapShotDevice.getSnapsForContext(contextId);
        } catch (SnapShotExplorerException e) {
            errorMessageFrame.newErrorDetected(e, true);
            errorMessageFrame.newMessageDetected("Error with the command getAllSnapsForContext \n"
                    + "context ID = " + contextId, true);
        }
        return snapsForContext;
    }
    
    public List<String> getAttributeListForContext(int contextID) {
        List<String> attributeListForContext = null;
        try {
            attributeListForContext = snapShotDevice.getAttributeListForContext(contextID);
        } catch (SnapShotExplorerException e) {
            errorMessageFrame.newErrorDetected(e, true);
            errorMessageFrame.newMessageDetected("Error with the command getAttributeListForContext \n"
                    + "context ID = " + contextID, true);
        }
        return attributeListForContext;       
    }
    
    public List<ContextModel> getContextListForAttribute(String attributeName) throws SnapShotExplorerException{
        List<ContextModel> contextListForAttribute = snapShotDevice.getContextListForAttribute(attributeName);
        return contextListForAttribute;      
    }
    
    public List<String> getSnapshotAttributeList() throws SnapShotExplorerException{
        List<String> snapshotAttributeList = snapShotDevice.getSnapshotAttributeList();
        return snapshotAttributeList;      
    }

    public List<SnapShotModel> getAllSnapsForContext(int contextId, List<SnapShotFilter> filter) throws SnapShotExplorerException{
        List<SnapShotModel> allSnapShot;
        List<SnapShotModel> filteredSnapShot = new ArrayList<SnapShotModel>();
        allSnapShot = getAllSnapsForContext(contextId);

      for (SnapShotModel snapShotModel : allSnapShot) {
        boolean matchingFilter = true;
        for (SnapShotFilter snapShotFilter : filter) {
            matchingFilter = snapShotFilter.isMatchingFilter(snapShotModel);
            if (!matchingFilter) {
                break;
            }
        }
        if (matchingFilter) {
            filteredSnapShot.add(snapShotModel);
        }
      }
        return filteredSnapShot;     
    }

    public ISnapShotFilter createContextFilter(FilterOperation operation, ComparedField field, Object comparedValue)
            throws SnapShotExplorerException {
        return new ContextFilter(operation, field, comparedValue);
    }

    public ISnapShotFilter createSnapShotFilter(FilterOperation operation, ComparedField field, Object comparedValue) {
        return new SnapShotFilter(operation, field, comparedValue);
    }
    
    public static void setRackCount(int arackCount) {
        rackCount = arackCount;
    }


    public static int getRackCount() {
        return rackCount;
    }
    
    public static void main(String[] args) {
        try {
            SnapShotController controller = new SnapShotController("archiving/snap-oracle/snapextractor.1");
            ArrayList<ContextFilter> arrayList = new ArrayList<ContextFilter>();
            ContextFilter contextFilter = new ContextFilter(FilterOperation.EQUALS, ComparedField.Author_FIELD, "katy");
            arrayList.add(contextFilter);
            List<ContextModel> allContext = controller.getAllContext(arrayList);
            System.out.println("allContext size=" + allContext.size());
            int id = 0;

            for (ContextModel model : allContext) {
                id = model.getId();
                System.out.println("Last context= " + model);
            }

            List<SnapShotModel> snapsForContext = controller.getAllSnapsForContext(id);
            for (SnapShotModel model : snapsForContext) {
                System.out.println("SnapShot = " + model);
            }

        } catch (SnapShotExplorerException e) {
            e.printStackTrace();
        }
    }
}
