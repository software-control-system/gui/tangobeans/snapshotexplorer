package fr.soleil.comete.bean.snapshotexplorer.view;

import java.awt.Color;
import java.awt.Component;
import java.util.List;

import javax.swing.JOptionPane;

import fr.soleil.comete.bean.snapshotexplorer.controller.SnapShotController;
import fr.soleil.comete.bean.snapshotexplorer.controller.SnapShotExplorerException;
import fr.soleil.comete.bean.snapshotexplorer.model.ContextModel;
import fr.soleil.comete.bean.snapshotexplorer.model.SnapShotExtractorDevice;
import fr.soleil.comete.bean.snapshotexplorer.view.ContextTable.ContextSelectionListener;
import fr.soleil.comete.bean.snapshotexplorer.view.SnapShotTable.SnapShotSelectionListener;
import fr.soleil.lib.project.swing.Splash;

public class SnapShotExplorer {
    private static SnapShotController m_controller = null;
    private static int selectID = SnapShotExplorerUtilities.NO_SELECTION;

    // ********* All different method for ContextTable *********\\

    // Method that will be used by MotorHoming
    public static int showSnapShotTableDialog(Component parentComponent, boolean showAttributes, String attributeName) {

        if (isSnapShotServiceAvailable()) {
            if (m_controller == null) {
                String deviceName = SnapShotExtractorDevice.getFirstAvailableDeviceName();
                if (SnapShotExtractorDevice.isSnapShotExtractorDeviceAvailable(deviceName)) {
                    m_controller = new SnapShotController(deviceName);
                }
            }
        }

        if (m_controller != null) {
            ContextSelectionListener contextListener = new ContextSelectionListener() {

                @Override
                public void selectedContextIDChanged(int contextId) {
                    SnapShotSelectionListener snapshotListener = new SnapShotSelectionListener() {

                        @Override
                        public void selectedSnapshotIDChanged(int snapshotId) {
                            selectID = snapshotId;
                        }
                    };
                    selectID = showSnapshotTableDialog(m_controller, snapshotListener, ContextTable.getDialog(), true,
                            contextId);
                }
            };
            showContextTableDialog(m_controller, contextListener, parentComponent, showAttributes, attributeName);
        } else {
            JOptionPane.showMessageDialog(parentComponent, "Snapshot service not available !",
                    "SnapshotExplorer Error", JOptionPane.ERROR_MESSAGE);
        }
        return selectID;
    }

    public static boolean isSnapShotServiceAvailable() {
        return SnapShotExtractorDevice.isSnapShotExtractorAvailable();
    }

    public static boolean isAttributeIsInContext(String attributeName) {
        boolean isIn = false;

        if (attributeName != null) {

            if (m_controller == null) {
                String firstAvailableDeviceName = SnapShotExtractorDevice.getFirstAvailableDeviceName();
                if (firstAvailableDeviceName != null) {
                    m_controller = new SnapShotController(firstAvailableDeviceName);
                }
            }

            if (m_controller != null) {
                try {
                    List<ContextModel> contextListForAttribute = m_controller.getContextListForAttribute(attributeName);
                    isIn = contextListForAttribute != null && !contextListForAttribute.isEmpty();
                } catch (SnapShotExplorerException e) {
                    isIn = false;
                }
            }

        }
        return isIn;
    }

    private static int showContextTableDialog(final SnapShotController controller, ContextSelectionListener listener) {
        return showContextTableDialog(controller, listener, null, true, null);
    }

    private static int showContextTableDialog(final SnapShotController controller, ContextSelectionListener listener,
            Component parentComponent, boolean showAttributes, String attributeName) {
        SnapShotExplorer.m_controller = controller;
        return ContextTable.showContextTableDialog(m_controller, listener, parentComponent, showAttributes,
                attributeName);
    }

    // ********* All different method for SnapshotTable *********\\

    public static int showSnapshotTableDialog(final SnapShotController controller, SnapShotSelectionListener listener,
            Component parentComponent, boolean showFilterPanel, int contextId) {
        return SnapShotTable.showSnapshotTableDialog(controller, listener, parentComponent, showFilterPanel, contextId);
    }

    public static int showSnapshotTableDialog(Component parentComponent, boolean showFilterPanel, int contextId) {
        return showSnapshotTableDialog(m_controller, null, parentComponent, showFilterPanel, contextId);
    }

    /*
    public static int showSnapshotTableDialog(SnapShotSelectionListener listener, boolean showFilterPanel, int contextId) {
        return showSnapshotTableDialog(m_controller, listener, null, showFilterPanel, contextId);
    }
    */
    public static void main(String[] args) {
        // Display Splash screen
        Splash splash = new Splash(SnapShotExplorerUtilities.ICONS.getIcon("generic.splash"), Color.WHITE);
        splash.setTitle("Snapshot Explorer application");
        splash.setCopyright("(c) Synchrotron Soleil 2016");
        splash.setAlwaysOnTop(true);
        splash.setMessage("Starting application...");
        splash.setAlwaysOnTop(false);
        splash.progress(5);
        splash.setMessage("Create Frame application ...");

        // Read device name
        String deviceName = null;
        splash.progress(10);
        splash.setMessage("Read the arguments ...");
        if (args != null && args.length > 0) {
            deviceName = args[0];
        }

        if (deviceName == null || deviceName.isEmpty()) {
            splash.progress(35);
            splash.setMessage("Search for a SnapExtractor device");
            String[] deviceList = SnapShotExtractorDevice.getDeviceNameList();
            if (deviceList != null) {
                if (deviceList.length == 1) {
                    // Take the first deviceName avaible in the deviceList
                    deviceName = deviceList[0];
                    splash.setMessage("One SnapExtractor device found : " + deviceName);
                } else if (deviceList.length > 1) {
                    splash.setMessage("Several SnapExtractor device found");
                    splash.progress(40);
                    String message = "Select a SnapExtractor device";
                    String title = "SnapExtractor selection";
                    int messageType = JOptionPane.QUESTION_MESSAGE;
                    Object selected = JOptionPane.showInputDialog(splash, message, title, messageType, null,
                            deviceList, deviceList[0]);
                    if (selected != null) {
                        deviceName = selected.toString();
                        splash.progress(60);
                        splash.setMessage("SnapExtractor device selected " + deviceName);
                    }
                }
            }
        }

        if (deviceName == null || deviceName.isEmpty()
                || !SnapShotExtractorDevice.isSnapShotExtractorDeviceAvailable(deviceName)) {
            splash.progress(70);
            String errorMessage = deviceName != null ? deviceName + " is not running !"
                    : "No SnapExtractor device found or device ";
            splash.setMessage(errorMessage);
            JOptionPane.showMessageDialog(splash, errorMessage, "SnapshotExplorer Error", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        } else {// Device found
            splash.progress(80);
            splash.setMessage("Create Snapshot Explorer Panel");
            splash.progress(90);
            splash.setMessage("Connexion on device SnapExtractor " + deviceName);
            final SnapShotController controller = new SnapShotController(deviceName);
            splash.progress(100);
            splash.setVisible(false);
            ContextSelectionListener contextListener = new ContextSelectionListener() {

                @Override
                public void selectedContextIDChanged(int contextId) {
                    System.out.println("contextId= " + contextId);
                    SnapShotSelectionListener snapshotListener = new SnapShotSelectionListener() {

                        @Override
                        public void selectedSnapshotIDChanged(int snapshotId) {
                            System.out.println("snapshotId = " + snapshotId);
                        }
                    };
                    SnapShotExplorer.showSnapshotTableDialog(controller, snapshotListener, ContextTable.getDialog(),
                            true, contextId);
                }
            };
            SnapShotExplorer.showContextTableDialog(controller, contextListener);
        }
    }
}
