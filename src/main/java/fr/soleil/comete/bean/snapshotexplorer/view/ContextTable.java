package fr.soleil.comete.bean.snapshotexplorer.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SortOrder;
import javax.swing.border.TitledBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import org.jdesktop.swingx.JXTable;

import fr.soleil.comete.bean.snapshotexplorer.controller.ISnapShotFilter.ComparedField;
import fr.soleil.comete.bean.snapshotexplorer.controller.SnapShotController;
import fr.soleil.comete.bean.snapshotexplorer.model.ContextModel;
import fr.soleil.comete.bean.snapshotexplorer.model.ExplorerModel;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.lib.project.swing.table.ITableButtonActionListener;
import fr.soleil.lib.project.swing.table.TableButtonRendererEditor;
import fr.soleil.lib.project.swing.table.TableEvent;

public class ContextTable extends AbstractTangoBox {

    private static final long serialVersionUID = -4307114819611093432L;
    private static final int ATTRIBUTE_COL_INDEX = 6;
    private static final int SELECTID_COL_INDEX = 7;

    // Functional member
    private static SnapShotController m_controller = null;
    private final List<ContextSelectionListener> selectionContextList;
    private List<ContextModel> data = null;
    private int m_ContextId;

    // GUI Member
    private JTable table;
    private ContextFilterPanel contextFilterPanel = null;
    private TableModel tableModel = null;
    private JScrollPane centerPanel;
    private TableButtonRendererEditor buttonSendID = null;
    private TableButtonRendererEditor buttonAttributeList = null;
    private JPanel mainPanel = null;
    private JPanel southPanel = null;
    private JButton cancelButton = null;
    private TitledBorder titled = null;

    // Attribute Frame
    private JFrame attributeFrame;
    private JPanel attributePanel;
    private JTextArea attributeTextArea;

    // Show ContexTable
    private static JDialog m_dialog;
    private static ContextTable m_contextTable; 
    
    public interface ContextSelectionListener {
        public void selectedContextIDChanged(int contextId);
    }

    public ContextTable(final SnapShotController controller) {
        ContextTable.m_controller = controller;
        selectionContextList = new ArrayList<ContextTable.ContextSelectionListener>();
        setLayout(new BorderLayout());
        initComponent();
    }

    @Override
    public void setModel(String deviceName) {
        data = m_controller.getAllContext();
        tableModel.fireTableDataChanged();
    }

    private void initComponent() {
        // Create Table
        setLayout(new BorderLayout());
        tableModel = new TableModel();
        table = new JXTable(tableModel);
        ((JXTable) table).setColumnControlVisible(false);
        ((JXTable) table).setDragEnabled(false);
        ((JXTable) table).setSortable(true);
        ((JXTable) table).getTableHeader().setReorderingAllowed(false);
        ((JXTable) table).setSortOrder(ExplorerModel.DATE_INDEX, SortOrder.DESCENDING);

        mainPanel = new JPanel();
        southPanel = new JPanel();

        // Set Size to ID Column
        TableColumn columnID = table.getColumnModel().getColumn(ExplorerModel.ID_INDEX);
        columnID.setMaxWidth(30);

        // Create button renderer for attribute column
        buttonAttributeList = new TableButtonRendererEditor();
        JButton attributebutton = buttonAttributeList.getButton();
        attributebutton.setIcon(SnapShotExplorerUtilities.applicationIcon);
        attributebutton.setToolTipText("Show attributes for selected context");
        buttonAttributeList.setCellRendererEditor(table.getColumnModel().getColumn(ATTRIBUTE_COL_INDEX));
        buttonAttributeList.addTableButtonActionListener(new ITableButtonActionListener() {
            @Override
            public void actionPerformed(TableEvent event) {

                int contextID = getContextID(event.getRow());
                List<String> attributeListForContext = getAttributeListForContext(contextID);
                showAttributesListForContextDialog();
                for (String attribute : attributeListForContext) {
                    attributeTextArea.append(attribute + "\n");
                }
            }
        });

        // Add column for selection ID
        TableColumn selectLineColumn = new TableColumn();
        selectLineColumn.setHeaderValue("Select line");
        table.getColumnModel().addColumn(selectLineColumn);
        selectLineColumn.setMaxWidth(70);
        selectLineColumn.setModelIndex(SELECTID_COL_INDEX);

        // Create button renderer for contextId selection column
        buttonSendID = new TableButtonRendererEditor();
        JButton buttonID = buttonSendID.getButton();
        buttonID.setIcon(SnapShotExplorerUtilities.arrowIcon);
        buttonID.setToolTipText("Return selected Context ID");
        buttonSendID.setCellRendererEditor(selectLineColumn);

        buttonSendID.addTableButtonActionListener(new ITableButtonActionListener() {
            @Override
            public void actionPerformed(TableEvent event) {
                m_ContextId = getContextID(event.getRow());
                notifySelectionChanged(m_ContextId);
            }
        });

        mainPanel.setLayout(new BorderLayout());
        centerPanel = new JScrollPane(table);
        showFilterPanel(true);

        mainPanel.add(centerPanel, BorderLayout.CENTER);

        add(mainPanel, BorderLayout.CENTER);
    }

    private void showButtonCancel(Component parent) {
            if (titled == null) {
                titled = new TitledBorder("");
            }
            if (cancelButton == null) {
                cancelButton = new JButton("Cancel");
                cancelButton.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        m_ContextId = SnapShotExplorerUtilities.NO_SELECTION;
                        m_dialog.dispose();
                    }
                });
            }
            southPanel.setBorder(titled);
            southPanel.add(cancelButton);
            add(southPanel, BorderLayout.SOUTH);
    }

    private void showAttributesListForContextDialog() {
        if (attributeFrame == null) {
            attributeFrame = new JFrame();
        }
        if (attributePanel == null) {
            attributePanel = new JPanel();
        }
        if (attributeTextArea == null) {
            attributeTextArea = new JTextArea();
        }
        attributeTextArea.setText("");

        attributePanel.setLayout(new BorderLayout());
        attributePanel.add(attributeTextArea, BorderLayout.CENTER);

        attributeFrame.setVisible(true);
        attributeFrame.setLocationRelativeTo(null);
        attributeFrame.setSize(400, 400);
        attributeFrame.setTitle("Attribute list for context");
        attributeFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        attributeFrame.setAlwaysOnTop(true);
        attributeFrame.add(attributePanel);
    }

    public static int showContextTableDialog(final SnapShotController acontroller, ContextSelectionListener listener,
            Component parentComponent, boolean showAttributes, String attributeName) {

        String deviceName = acontroller.getDeviceName();

        if (m_contextTable == null) {
            m_contextTable = new ContextTable(acontroller);
            m_contextTable.setModel(deviceName);
        }

        if (m_dialog == null) {
            m_dialog = new JDialog();
            m_dialog.setModal(parentComponent != null);
            m_dialog.setAlwaysOnTop(true);
            m_dialog.setContentPane(m_contextTable);
            m_dialog.setTitle("Context Table " + ">> " + deviceName);
            if (parentComponent == null) {
                m_dialog.setLocationRelativeTo(parentComponent);
                m_dialog.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                });
            }
        }

        if (listener != null) {
            m_contextTable.addContextSelectionListener(listener);
        }
        if (parentComponent != null) {
            m_contextTable.showButtonCancel(parentComponent);
        }
        m_contextTable.showAttributesFilter(showAttributes);
        m_contextTable.setAttributeFilter(attributeName);
        m_dialog.toFront();
        m_dialog.pack();
        m_dialog.setVisible(true);

        return m_contextTable.getSelectedContextId();
    }
    
    public static void closeDialog(){
        if(m_dialog != null){
            m_dialog.dispose();
        }
    }

    private void showFilterPanel(boolean isVisible) {
        if (isVisible) {
            if (contextFilterPanel == null) {
                contextFilterPanel = new ContextFilterPanel(this);
            }
            mainPanel.add(contextFilterPanel, BorderLayout.SOUTH);
        }
    }

    private class TableModel extends AbstractTableModel {

        private static final long serialVersionUID = 1029607279384148542L;

        @Override
        public int getColumnCount() {
            return ComparedField.values().length + 1;
        }

        @Override
        public String getColumnName(int column) {
            String columnName = "Attributes";
            if (column < ComparedField.values().length) {
                columnName = ComparedField.values()[column].toString();
            }
            return columnName;
        }

        @Override
        public int getRowCount() {
            return data != null ? data.size() : 0;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object object = null;

            if (data != null && rowIndex < data.size() && columnIndex != data.size() + 1) {
                ContextModel contextModel = data.get(rowIndex);
                object = contextModel.getValue(columnIndex);
            }
            return object;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return (columnIndex == ATTRIBUTE_COL_INDEX || columnIndex == SELECTID_COL_INDEX);
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            Class<?> clazz = String.class;
            switch (columnIndex) {
                case ExplorerModel.ID_INDEX:
                    clazz = Integer.class;
                    break;
                case ExplorerModel.DATE_INDEX:
                    clazz = Date.class;
                    break;
                default:
                    super.getColumnClass(columnIndex);
            }
            return clazz;
        }
    }

    @Override
    protected void clearGUI() {

    }

    @Override
    protected void refreshGUI() {

    }

    @Override
    protected void onConnectionError() {
    }

    @Override
    protected void savePreferences(Preferences preferences) {
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
    }

    public void addContextSelectionListener(ContextSelectionListener listener) {
        if (!selectionContextList.contains(listener)) {
            selectionContextList.add(listener);
        }
    }

    public void removeContextSelectionListener(ContextSelectionListener listener) {
        if (selectionContextList.contains(listener)) {
            selectionContextList.remove(listener);
        }
    }

    private void notifySelectionChanged(int selectedContextID) {
        for (ContextSelectionListener listener : selectionContextList) {
            listener.selectedContextIDChanged(selectedContextID);
        }
    }

    public void resetData() {
        if (m_controller != null) {
            data = m_controller.getAllContext();
            tableModel.fireTableDataChanged();
        }
    }

    public void resfreshPanel() {
        contextFilterPanel.revalidate();
        contextFilterPanel.repaint();
    }

    private int getContextID(int row) {
        int id = SnapShotExplorerUtilities.NO_SELECTION;
        if (table != null) {
            id = (Integer) table.getValueAt(row, ExplorerModel.ID_INDEX);
        }
        return id;
    }

    private List<String> getAttributeListForContext(int contextID) {
        List<String> attributeList = new ArrayList<String>();
        if (contextID >= 0) {
            attributeList = m_controller.getAttributeListForContext(contextID);
        }
        return attributeList;
    }

    public int[] getSelectedContextIdList() {
        int[] selectedId = null;
        if (table != null) {
            int[] selectedRows = table.getSelectedRows();
            if (selectedRows != null) {
                selectedId = new int[selectedRows.length];
                for (int i = 0; i < selectedId.length; i++) {
                    int indexRow = selectedRows[i];
                    selectedId[i] = (Integer) table.getValueAt(indexRow, ExplorerModel.ID_INDEX);
                }
            }
        }
        return selectedId;
    }

    public int getSelectedContextId() {
        int[] selectedIdList = getSelectedContextIdList();
        int selectedId = SnapShotExplorerUtilities.NO_SELECTION;
        if (selectedIdList != null && selectedIdList.length > 0) {
            selectedId = selectedIdList[0];
        }
        return selectedId;
    }

    public SnapShotController getController() {
        return m_controller;
    }

    public List<ContextModel> getData() {
        return data;
    }

    public void setData(List<ContextModel> data) {
        this.data = data;
        tableModel.fireTableDataChanged();
    }

    public void setAttributeFilter(String filterApply) {
        contextFilterPanel.setAttributeFilter(filterApply);
    }

    public void showAttributesFilter(boolean showAttributesFilter) {
        contextFilterPanel.showAttributeFilter(showAttributesFilter);
    }

    public JTable getTable() {
        return table;
    }

    public static JDialog getDialog() {
        return m_dialog;
    }
 
}
