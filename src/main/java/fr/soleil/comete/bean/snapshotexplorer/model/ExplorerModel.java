package fr.soleil.comete.bean.snapshotexplorer.model;

import java.util.Date;

public class ExplorerModel {

    public static final int ID_INDEX = 0;
    public static final int DATE_INDEX = 1;
    public static final int DESCRIPTION_INDEX = 2;

    private final int id;

    private final String description;

    private final Date date;

    public ExplorerModel(int id, String description, Date date) {
        super();
        this.id = id;
        this.description = description;
        this.date = date;
    }

    public Object getValue(int columnIndex) {
        Object avalue = null;
        switch (columnIndex) {
            case ID_INDEX:
                avalue = getId();
                break;
            case DATE_INDEX:
                avalue = getDate();
                break;
            case DESCRIPTION_INDEX:
                avalue = getDescription();
                break;
        }
        return avalue;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID =" + id + "\n");
        builder.append("Description = " + description + "\n");
        builder.append("Date = " + SnapShotExtractorDevice.DATE_FORMATER.format(date) + "\n");
        return builder.toString();
    }
}
