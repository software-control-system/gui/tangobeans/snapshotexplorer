package fr.soleil.comete.bean.snapshotexplorer.view;

import java.text.SimpleDateFormat;

import javax.swing.ImageIcon;

import fr.soleil.lib.project.swing.icons.Icons;

public class SnapShotExplorerUtilities {
    
    public final static Icons ICONS = new Icons("fr.soleil.comete.bean.snapshotexplorer.icons");
    
    public static final ImageIcon applicationIcon = new ImageIcon(
            SnapShotExplorerUtilities.class.getResource("/com/famfamfam/silk/application.png"));
    

    public static final ImageIcon arrowIcon = new ImageIcon(
            SnapShotExplorerUtilities.class.getResource("/com/famfamfam/silk/arrow_right.png"));
    
     
    public static final ImageIcon zoomIcon = new ImageIcon(
            SnapShotExplorerUtilities.class.getResource("/com/famfamfam/silk/zoom.png"));
    
    public static final int NO_SELECTION = -1;
    
    public static final SimpleDateFormat DATE_FORMATER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
}
