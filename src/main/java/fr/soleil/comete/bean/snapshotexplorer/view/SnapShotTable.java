package fr.soleil.comete.bean.snapshotexplorer.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SortOrder;
import javax.swing.border.TitledBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import org.jdesktop.swingx.JXTable;

import fr.soleil.comete.bean.snapshotexplorer.controller.ISnapShotFilter.ComparedSnapField;
import fr.soleil.comete.bean.snapshotexplorer.controller.SnapShotController;
import fr.soleil.comete.bean.snapshotexplorer.model.ExplorerModel;
import fr.soleil.comete.bean.snapshotexplorer.model.SnapShotExtractorDevice;
import fr.soleil.comete.bean.snapshotexplorer.model.SnapShotModel;
import fr.soleil.lib.project.swing.table.ITableButtonActionListener;
import fr.soleil.lib.project.swing.table.TableButtonRendererEditor;
import fr.soleil.lib.project.swing.table.TableEvent;

public class SnapShotTable extends JPanel {

    private static final long serialVersionUID = -3517418029454040315L;

    private static final int SELECTID_COL_INDEX = 3;

    // Functional member
    private SnapShotController controller = null;
    private List<SnapShotModel> data = null;
    private List<SnapShotSelectionListener> selectionSnapshotList;
    private DateCellRenderer dateCellRenderer;
    private int contextId;
    private int snapshotId;

    // GUI member
    private JTable table;
    private TableModel tableModel = null;
    private SnapShotFilterPanel snapShotFilterPanel = null;
    private JPanel northPanel = null;
    private JLabel northLabel = null;
    private TableButtonRendererEditor buttonSendID = null;
    private JScrollPane centerPanel = null;
    private JButton cancelButton = null;
    private JButton okButton = null;
    private JPanel southPanel = null;
    private JPanel mainPanel = null;
    private TitledBorder titled = null;

    // Show Snapshot table
    private static JDialog m_dialog;
    private static SnapShotTable m_snapshotTable;

    public interface SnapShotSelectionListener {
        public void selectedSnapshotIDChanged(int snapshotId);
    }

    public SnapShotTable(SnapShotController controller) {
        setLayout(new BorderLayout());
        selectionSnapshotList = new ArrayList<SnapShotTable.SnapShotSelectionListener>();
        initComponent();
    }

    public void setModel(String deviceName) {
        controller = new SnapShotController(deviceName);
    }

    private void initComponent() {
        setLayout(new BorderLayout());
        // Create table
        tableModel = new TableModel();
        table = new JXTable(tableModel);
        ((JXTable) table).setColumnControlVisible(false);
        ((JXTable) table).setDragEnabled(false);
        ((JXTable) table).setSortable(true);
        ((JXTable) table).getTableHeader().setReorderingAllowed(false);
        ((JXTable) table).setSortOrder(ExplorerModel.DATE_INDEX, SortOrder.DESCENDING);

        southPanel = new JPanel();
        mainPanel = new JPanel();

        titled = new TitledBorder("");

        // Set size to ID column
        TableColumn columnID = table.getColumnModel().getColumn(ExplorerModel.ID_INDEX);
        columnID.setMaxWidth(30);

        // Create button for select ID
        buttonSendID = new TableButtonRendererEditor();
        JButton buttonID = buttonSendID.getButton();
        buttonID.setIcon(SnapShotExplorerUtilities.arrowIcon);
        buttonID.setToolTipText("Return selected Context ID");
        buttonSendID.setCellRendererEditor(table.getColumnModel().getColumn(SELECTID_COL_INDEX));
        buttonSendID.addTableButtonActionListener(new ITableButtonActionListener() {
            @Override
            public void actionPerformed(TableEvent event) {
                snapshotId = getSnapshotID(event.getRow());
                notifySelectionChanged(snapshotId);
                m_dialog.dispose();
                ContextTable.closeDialog();
            }
        });

        // Set size to select ID column
        TableColumn columnSelectID = table.getColumnModel().getColumn(SELECTID_COL_INDEX);
        columnSelectID.setMaxWidth(80);

        // Add dateCellRendere to columnDate
        TableColumn columnDate = table.getColumnModel().getColumn(ExplorerModel.DATE_INDEX);
        dateCellRenderer = new DateCellRenderer();
        columnDate.setCellRenderer(dateCellRenderer);

        // Create button ok and cancel
        okButton = new JButton("Ok");
        okButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = table.getSelectedRow();
                TableColumn columnID = table.getColumnModel().getColumn(ExplorerModel.ID_INDEX);
                int modelIndex = columnID.getModelIndex();
                if (selectedRow > -1 && modelIndex > -1) {
                    Object valueAt = table.getValueAt(selectedRow, modelIndex);
                    if (valueAt instanceof Integer) {
                        snapshotId = (Integer) valueAt;
                        snapshotId = getSelectedSnapshotID();
                        notifySelectionChanged(snapshotId);
                        m_dialog.dispose();
                        ContextTable.closeDialog();
                    }
                } else {
                    snapshotId = SnapShotExplorerUtilities.NO_SELECTION;
                    notifySelectionChanged(snapshotId);
                    m_dialog.dispose();
                }
            }
        });

        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                snapshotId = SnapShotExplorerUtilities.NO_SELECTION;
                notifySelectionChanged(snapshotId);
                m_dialog.dispose();
            }
        });

        mainPanel.setLayout(new BorderLayout());
        setLayout(new BorderLayout());

        southPanel.setBorder(titled);
        southPanel.add(okButton);
        southPanel.add(cancelButton);

        centerPanel = new JScrollPane(table);

        mainPanel.add(centerPanel);

        showFilterPanel(true);
        add(mainPanel, BorderLayout.CENTER);
        add(southPanel, BorderLayout.SOUTH);
    }

    public static int showSnapshotTableDialog(SnapShotController acontroller, SnapShotSelectionListener listener,
            Component parentComponent, boolean showFilterPanel, int contextId) {
        String deviceName = acontroller.getDeviceName();

        if (m_snapshotTable == null) {
            m_snapshotTable = new SnapShotTable(acontroller);
            m_snapshotTable.setModel(deviceName);
            m_snapshotTable.showFilterPanel(showFilterPanel);
        }

        m_snapshotTable.setContextID(contextId);

        if (m_dialog == null) {
            m_dialog = new JDialog();
            m_dialog.setModal(true);
            m_dialog.setAlwaysOnTop(true);
            m_dialog.setContentPane(m_snapshotTable);
            m_dialog.setTitle("Snapshot Table " + ">> " + deviceName);
            m_dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        }

        if (listener != null) {
            m_snapshotTable.addSnapshotSelectionListener(listener);
        }

        m_dialog.toFront();
        m_dialog.setLocationRelativeTo(parentComponent);
        m_dialog.pack();
        m_dialog.setVisible(true);

        return m_snapshotTable.getSelectedSnapshotID();
    }

    private void showFilterPanel(boolean isVisible) {
        if (isVisible) {
            snapShotFilterPanel = new SnapShotFilterPanel(this);
            mainPanel.add(snapShotFilterPanel, BorderLayout.SOUTH);
        }
    }

    private void contextLabel(String contextNameLabel, int contextIDForLabel) {
        if (northPanel == null) {
            northPanel = new JPanel();
        }
        if (contextNameLabel == null) {
            contextNameLabel = "null";
        }
        if (northLabel == null) {
            northLabel = new JLabel();
        }
        String contextIdAndContextName = null;
        contextIdAndContextName = "Context ID : " + contextIDForLabel + "  " + " Context name : " + contextNameLabel;
        northLabel.setText(contextIdAndContextName);
        northPanel.add(northLabel);
        add(northPanel, BorderLayout.NORTH);
    }

    private class TableModel extends AbstractTableModel {

        private static final long serialVersionUID = -3010891284792713488L;

        @Override
        public int getColumnCount() {
            return ComparedSnapField.values().length + 1;
        }

        @Override
        public String getColumnName(int column) {
            String columnName = "Select line";
            if (column < ComparedSnapField.values().length) {
                columnName = ComparedSnapField.values()[column].toString();
            }
            return columnName;
        }

        @Override
        public int getRowCount() {
            return data != null ? data.size() : 0;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object object = null;
            if (data != null && rowIndex < data.size() && columnIndex != data.size() + 1) {
                SnapShotModel snapShotModel = data.get(rowIndex);
                object = snapShotModel.getValue(columnIndex);
            }
            return object;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            if (columnIndex == SELECTID_COL_INDEX) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            Class<?> clazz = String.class;
            switch (columnIndex) {
                case ExplorerModel.ID_INDEX:
                    clazz = Integer.class;
                    break;
                case ExplorerModel.DATE_INDEX:
                    clazz = Date.class;
                    break;
                default:
                    super.getColumnClass(columnIndex);
            }
            return clazz;
        }
    }

    // Renderer for watch the hours of Snapshot
    public class DateCellRenderer extends DefaultTableCellRenderer {

        private static final long serialVersionUID = 6254356495216790678L;

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column) {
            super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if (value instanceof Date) {
                String strDate = SnapShotExtractorDevice.DATE_FORMATER.format((Date) value);
                this.setText(strDate);
            }
            return this;
        }
    }

    public void addSnapshotSelectionListener(SnapShotSelectionListener listener) {
        if (!selectionSnapshotList.contains(listener)) {
            selectionSnapshotList.add(listener);
        }
    }

    public void removeContextSelectionListener(SnapShotSelectionListener listener) {
        if (selectionSnapshotList.contains(listener)) {
            selectionSnapshotList.remove(listener);
        }
    }

    private void notifySelectionChanged(int selectedSnapshotID) {
        for (SnapShotSelectionListener listener : selectionSnapshotList) {
            listener.selectedSnapshotIDChanged(selectedSnapshotID);
        }
    }

    public void resetData() {
        if (controller != null) {
            data = controller.getAllSnapsForContext(getContextID());
            tableModel.fireTableDataChanged();
        }
    }

    public void resfreshPanel() {
        snapShotFilterPanel.revalidate();
        snapShotFilterPanel.repaint();
    }

    public int setContextID(int contextID) {
        contextId = contextID;
        String contextName = null;
        data = controller.getAllSnapsForContext(contextID);
        contextName = controller.getContext(contextID).getContextName();
        contextLabel(contextName, contextID);
        Collections.reverse(data);
        tableModel.fireTableDataChanged();
        return contextID;
    }

    public int getContextID() {
        return contextId;
    }

    private int getSnapshotID(int row) {
        int id = 0;
        if (table != null) {
            SnapShotModel snapShotModel = data.get(row);
            if (snapShotModel != null) {
                id = snapShotModel.getId();
            }
        }
        return id;
    }

    private int[] getSelectedSnapshotIDList() {
        int[] selectedId = null;
        if (table != null) {
            int[] selectedRows = table.getSelectedRows();
            if (selectedRows != null) {
                selectedId = new int[selectedRows.length];
                for (int i = 0; i < selectedId.length; i++) {
                    int indexRow = selectedRows[i];
                    selectedId[i] = (Integer) table.getValueAt(indexRow, ExplorerModel.ID_INDEX);
                }
            }
        }
        return selectedId;
    }

    public int getSelectedSnapshotID() {
        int[] selectedIdList = getSelectedSnapshotIDList();
        int selectedId = SnapShotExplorerUtilities.NO_SELECTION;
        if (selectedIdList != null && selectedIdList.length > 0) {
            selectedId = selectedIdList[0];
        }
        return selectedId;
    }

    public SnapShotController getController() {
        return controller;
    }

    public List<SnapShotModel> getData() {
        return data;
    }

    public void setData(List<SnapShotModel> data) {
        this.data = data;
        tableModel.fireTableDataChanged();
    }
}
