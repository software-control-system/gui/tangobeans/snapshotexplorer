package fr.soleil.comete.bean.snapshotexplorer.model;

import java.util.Date;

public class ContextModel extends ExplorerModel {

    public static final int NAME_INDEX = 3;
    public static final int AUTHOR_INDEX = 4;
    public static final int REASON_INDEX = 5;
    
    private final String name;

    private final String author;

    private final String reason;

    public ContextModel(int id, String description, Date date, String name, String author, String reason) {
        super(id, description, date);
        this.name = name;
        this.author = author;
        this.reason = reason;
    }

    public String getContextName() {
        return name;
    }

    public String getContextAuthor() {
        return author;
    }

    public String getContextReason() {
        return reason;
    }
  

    @Override
    public Object getValue(int columnIndex) {
        Object value = super.getValue(columnIndex);
        if (value == null) {
            switch (columnIndex) {
                case NAME_INDEX:
                    value = getContextName();
                    break;
                case AUTHOR_INDEX:
                    value = getContextAuthor();
                    break;
                case REASON_INDEX:
                    value = getContextReason();
                    break;
            }
        }
        return value;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Name = " + name + "\n");
        builder.append("Author = " + author + "\n");
        builder.append("Reason = " + reason + "\n");
        builder.append(super.toString());
        return builder.toString();
    }
}
